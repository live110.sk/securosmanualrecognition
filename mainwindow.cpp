#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QButtonGroup"
#include "QObject"
#include "QDebug"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QTimer>
#include <QDateTime>
#include <QClipboard>

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {
    ui->setupUi(this);
    connect(ui->pushButton_1, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_9, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_0, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_T, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_Y, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_O, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_P, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_A, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_E, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_H, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_K, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_X, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_C, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_B, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_minus, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_M, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_I, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
    connect(ui->pushButton_ok, SIGNAL(clicked()), this, SLOT(pushButtonOkClicked()));
    connect(ui->pushButton_del, SIGNAL(clicked()), this, SLOT(pushButtonDelClicked()));
    connectToDatabase();
    qDebug() << "A258XO21:" << getPlateInfo("A258XO21");
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(tickOneSecond()));
    timer->start(1000);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::pushButtonClicked() {
    QPushButton *clickedButton = qobject_cast<QPushButton *>(sender());
    ui->lineEdit_plate->setText(ui->lineEdit_plate->text() + clickedButton->text());
    lastGuiActivityTime = elapsedSeconds;
    updatePlateInfo();
}

void MainWindow::pushButtonDelClicked() {
    if (ui->lineEdit_plate->text().size() > 0)
        ui->lineEdit_plate->setText(ui->lineEdit_plate->text().chopped(1));
    lastGuiActivityTime = elapsedSeconds;
    updatePlateInfo();
}

void MainWindow::pushButtonOkClicked() {
    QSqlQuery myQuery(db);
    myQuery.prepare(
            "INSERT INTO tobackup.t_log(tid, lpr_id, lpr_name, lpr_type, camera_host, reverse, recognition_quality,plate_recognized, country,"
            "country_standard, time_enter, time_best, time_leave, speed,short_summary,state,recorded_camera_list,plate_rect) "
            "VALUES (tobackup.gen_log_key(), '1','KPP_MANUAL', 'LPR_CAM_LITE', 'GATEVIDEO', '0', '500',:plate,'RUSSIA', 'GOST30',"
            " now(),now(),now(),'0.11',:information,'100','1,2,3,4','0.724219,0.34875,0.809375,0.3775')");
    myQuery.bindValue(":plate", ui->lineEdit_plate->text());
    myQuery.bindValue(":information", getPlateInfo(ui->lineEdit_plate->text()));
    myQuery.exec();
    ui->lineEdit_plate->setText("");
    ui->textEdit_plateInfo->append("\n Номер РАСПОЗНАН ВРУЧНУЮ");
    lastGuiActivityTime = elapsedSeconds;
}

void MainWindow::connectToDatabase() {
    db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("192.168.3.254");
    db.setDatabaseName("auto");
    db.setUserName("postgres");
    db.setPassword("postgres");
    if (!db.open()) {
        qCritical() << "Can not open database" << db.lastError().text();;
        return;
    }
    qInfo() << tr("database opened ") << db.databaseName();

}

QString MainWindow::getPlateInfo(QString plate) {
    QSqlQuery myQuery(db);
    myQuery.prepare("WITH requested_plate(plate) AS (SELECT iss.translit_plate(:plate)),\n"
                    "    requested_car(plate, car_id) AS (SELECT rp.plate, COALESCE(c.car_id, -1) FROM requested_plate rp LEFT JOIN iss.cars_1 c ON c.plate = rp.plate),\n"
                    "    actual_permit(permit_id, car_id, plate) AS (SELECT iss.get_permit_for_car(car_id, TIMESTAMP 'now'), car_id, plate FROM requested_car LIMIT 1),\n"
                    "    report(message) AS (\n"
                    "      SELECT message FROM\n"
                    "                          ((SELECT '(+)'||', '||CASE WHEN pmt.owner_object_type_code = 0 THEN p.family_name||' '||p.first_name ||(CASE WHEN p.middle_name NOTNULL THEN ' '||p.middle_name ELSE '' END)||(CASE WHEN pd.department_id ISNULL THEN '' ELSE ', '||pd.title END) WHEN pmt.owner_object_type_code = 1 THEN  d.title END||'.' message, 0 ordinal\n"
                    "                            FROM actual_permit ap\n"
                    "                                   LEFT JOIN iss.permits pmt ON ap.permit_id = pmt.permit_id\n"
                    "                                   LEFT JOIN iss.profiles p ON pmt.owner_object_type_code = 0 AND pmt.owner_object_id = p.profile_id\n"
                    "                                   LEFT JOIN iss.departments pd ON p.department_id = pd.department_id\n"
                    "                                   LEFT JOIN iss.departments d ON pmt.owner_object_type_code = 1 AND pmt.owner_object_id = d.department_id\n"
                    "                            WHERE ap.permit_id > 0)\n"
                    "                           UNION\n"
                    "                           (SELECT  (CASE WHEN c.owner_code = 0 THEN p.family_name||' '||p.first_name ||(CASE WHEN p.middle_name NOTNULL THEN ' '||p.middle_name ELSE '' END)||(CASE WHEN pd.department_id ISNULL THEN '' ELSE ', '||pd.title END) WHEN c.owner_code = 1 THEN  d.title END)||' - нет разрешения.', 1\n"
                    "                            FROM actual_permit ap\n"
                    "                                   LEFT JOIN iss.cars_1 c ON ap.car_id = c.car_id\n"
                    "                                   LEFT JOIN iss.profiles p ON c.owner_code = 0 AND c.owner_id = p.profile_id\n"
                    "                                   LEFT JOIN iss.departments pd ON p.department_id = pd.department_id\n"
                    "                                   LEFT JOIN iss.departments d ON c.owner_code = 1 AND c.owner_id = d.department_id\n"
                    "                            WHERE ap.permit_id = 0 AND ap.car_id > 0 AND c.owner_id > 0)\n"
                    "                           UNION\n"
                    "                           (SELECT 'нет разрешения.', 2\n"
                    "                            FROM actual_permit ap LEFT JOIN iss.cars_1 c ON ap.car_id = c.car_id\n"
                    "                            WHERE ap.permit_id = 0 AND (ap.car_id = 0 OR c.owner_id = 0))\n"
                    "                          ) x ORDER BY x.ordinal ASC LIMIT 1)\n"
                    "SELECT message FROM report;");
    myQuery.bindValue(":plate", plate);
    myQuery.exec();

    if (myQuery.next())
        return myQuery.value(0).toString();
    return "Владелец не найден или нет разрешения";
}

void MainWindow::updatePlateInfo() {

    bool nowGuiActive = lastGuiActivityTime + 10 > elapsedSeconds;
    if (nowGuiActive != isGuiActive) {
        if (isGuiActive)
            lineEditPlate = ui->lineEdit_plate->text();
        else
            ui->lineEdit_plate->setText(lineEditPlate);
    }
    isGuiActive = nowGuiActive;
    if (isGuiActive) {
        lineEditPlate = ui->lineEdit_plate->text();
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(lineEditPlate);
    } else {
        QDateTime qDateTime = QDateTime::currentDateTime();
        ui->lineEdit_plate->setText(qDateTime.toString("hh:mm:ss"));
    }
    ui->textEdit_plateInfo->setText(getPlateInfo(lineEditPlate));
    ui->lineEdit_plate->setFocus();

}

void MainWindow::tickOneSecond() {
    elapsedSeconds++;
    updatePlateInfo();
}
/*var sql = "INSERT INTO tobackup.t_log(tid, lpr_id, lpr_name, lpr_type, camera_host, reverse, recognition_quality,plate_recognized, country,";
sql=sql+"country_standard, time_enter, time_best, time_leave, speed,short_summary,state,recorded_camera_list,plate_rect)";
sql=sql+"VALUES (tobackup.gen_log_key(), ";
sql=sql+"'1','KPP_MANUAL', 'LPR_CAM_LITE', 'GATEVIDEO', '0', '500','";
sql=sql+number1;
sql=sql+"','RUSSIA', 'GOST30', now(),now(),now(),'0.11','"+information+"','100','1,2,3,4',";
sql=sql+"'0.724219,0.34875,0.809375,0.3775');";*/

/*WITH requested_plate(plate) AS (SELECT iss.translit_plate(?)),
requested_car(plate, car_id) AS (SELECT rp.plate, COALESCE(c.car_id, -1) FROM requested_plate rp LEFT JOIN iss.cars_1 c ON c.plate = rp.plate),
actual_permit(permit_id, car_id, plate) AS (SELECT iss.get_permit_for_car(car_id, TIMESTAMP 'now'), car_id, plate FROM requested_car LIMIT 1),
report(message) AS (
        SELECT message FROM
        ((SELECT '(+)'||', '||CASE WHEN pmt.owner_object_type_code = 0 THEN p.family_name||' '||p.first_name ||(CASE WHEN p.middle_name NOTNULL THEN ' '||p.middle_name ELSE '' END)||(CASE WHEN pd.department_id ISNULL THEN '' ELSE ', '||pd.title END) WHEN pmt.owner_object_type_code = 1 THEN  d.title END||'.' message, 0 ordinal
        FROM actual_permit ap
LEFT JOIN iss.permits pmt ON ap.permit_id = pmt.permit_id
LEFT JOIN iss.profiles p ON pmt.owner_object_type_code = 0 AND pmt.owner_object_id = p.profile_id
LEFT JOIN iss.departments pd ON p.department_id = pd.department_id
LEFT JOIN iss.departments d ON pmt.owner_object_type_code = 1 AND pmt.owner_object_id = d.department_id
WHERE ap.permit_id > 0)
UNION
(SELECT  (CASE WHEN c.owner_code = 0 THEN p.family_name||' '||p.first_name ||(CASE WHEN p.middle_name NOTNULL THEN ' '||p.middle_name ELSE '' END)||(CASE WHEN pd.department_id ISNULL THEN '' ELSE ', '||pd.title END) WHEN c.owner_code = 1 THEN  d.title END)||' - нет разрешения.', 1
FROM actual_permit ap
        LEFT JOIN iss.cars_1 c ON ap.car_id = c.car_id
LEFT JOIN iss.profiles p ON c.owner_code = 0 AND c.owner_id = p.profile_id
LEFT JOIN iss.departments pd ON p.department_id = pd.department_id
LEFT JOIN iss.departments d ON c.owner_code = 1 AND c.owner_id = d.department_id
WHERE ap.permit_id = 0 AND ap.car_id > 0 AND c.owner_id > 0)
UNION
(SELECT 'нет разрешения.', 2
FROM actual_permit ap LEFT JOIN iss.cars_1 c ON ap.car_id = c.car_id
WHERE ap.permit_id = 0 AND (ap.car_id = 0 OR c.owner_id = 0))
) x
        ORDER BY x.ordinal ASC
LIMIT 1)
//SELECT message FROM report;*/




