#pragma once

#include <QMainWindow>
#include <QtWidgets/QAbstractButton>
#include <QtSql/QSqlDatabase>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QSqlDatabase db;
    long elapsedSeconds, lastGuiActivityTime;
    bool isGuiActive;
    QString lineEditPlate;
//    QClipboard *clipboard;

public slots:

    void pushButtonClicked();
    void pushButtonDelClicked();
    void pushButtonOkClicked();
    void connectToDatabase();
    QString getPlateInfo(QString plate);
    void updatePlateInfo();
    void tickOneSecond();


};


